var express = require('express');
var router = express.Router();
var fs = require('fs');
var signUpFiledsData = require("../public/data/signUpFields_ver2.json");

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Link Staff' });
});

/* GET inters page. */
router.get('/intern', function(req, res, next) {
  fs.readFile(__dirname + "/../public/data/interns.json",function(err,content){
    if(err) throw err;
    let parseJson = JSON.parse(content);
    // parseJson.push(intern);
    // console.log(parseJson, "intern");

    res.render('intern', { interns: parseJson });
  })
});

/* GET studentss page. */
router.get('/students', function(req, res, next) {
  fs.readFile(__dirname + "/../public/data/students.json",function(err,content){
    if(err) throw err;
    let parseJson = JSON.parse(content);
    // parseJson.push(intern);
    // console.log(parseJson, "intern");

    res.render('students', { students: parseJson });
  })
});

/* GET others page. */
router.get('/others', function(req, res, next) {
  fs.readFile(__dirname + "/../public/data/others.json",function(err,content){
    if(err) throw err;
    let parseJson = JSON.parse(content);
    // parseJson.push(intern);
    // console.log(parseJson, "intern");

    res.render('others', { others: parseJson });
  })
});



/* GET sign up fields data. */
router.get('/signup/fieldsData/', function(req, res, next) {
  res.status(200).json(signUpFiledsData);
});

/* POST add an intern */
router.post('/signup/intern/', function(req, res, next) {
  let intern = req.body;
  
  fs.readFile(__dirname + "/../public/data/interns.json",function(err,content){
    if(err) throw err;
    let parseJson = JSON.parse(content);
    parseJson.push(intern);
    console.log(intern, "intern");

    fs.writeFile(__dirname + "/../public/data/interns.json",JSON.stringify(parseJson),function(err){
      if(err) throw err;
      else 
        res.status(200).json("success");
    })
  })

});

/* POST add an student  */
router.post('/signup/student/', function(req, res, next) {
  let student = req.body;
  
  fs.readFile(__dirname + "/../public/data/students.json",function(err,content){
    if(err) throw err;
    let parseJson = JSON.parse(content);
    parseJson.push(student);
    console.log(student, "student");

    fs.writeFile(__dirname + "/../public/data/students.json",JSON.stringify(parseJson),function(err){
      if(err) throw err;
      else 
        res.status(200).json("success");
    })
  });

});

/* POST add others  */
router.post('/signup/other/', function(req, res, next) {
  let other = req.body;
  
  fs.readFile(__dirname + "/../public/data/others.json",function(err,content){
    if(err) throw err;
    let parseJson = JSON.parse(content);
    parseJson.push(other);
    console.log(other, "other");

    fs.writeFile(__dirname + "/../public/data/others.json",JSON.stringify(parseJson),function(err){
      if(err) throw err;
      else 
        res.status(200).json("success");
    })
  });

});


/* POST check code  */
router.post('/verify', function(req, res, next) {
  let code = req.body.code;
  
  if (code == "1234")
  res.status(200).json("success");    
  else
  res.status(404).json("invalid code");

});

/* POST check  */
router.post('/check', function(req, res, next) {
  // let code = req.body.code;
  console.log(req.body);

  // req.send("ok")
  
  res.status(200).json({result: "success"});    
  // res.status(404).json("invalid code");

});


module.exports = router;
